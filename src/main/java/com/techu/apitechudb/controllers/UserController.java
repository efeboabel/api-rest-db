package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam(name="ord",required = false) String ord){
        System.out.println("getUsers");

        return new ResponseEntity<List<UserModel>>(
          this.userService.findAll(ord),
          HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("El id del usuario a buscar es: " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario No Encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("Este es el id del usuario a insertar" + user.getId());
        System.out.println("Este es el nombre del usuario a insertar" + user.getName());
        System.out.println("Este es la edad del usuario a insertar" + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<String> patchUser(@PathVariable String id, @RequestBody UserModel userData){
        System.out.println("patchUser");
        System.out.println("El id del Usuario a Actualizar es: " + id);
        System.out.println("El nombre del Usuario a Actualizar es: " + userData.getName());
        System.out.println("La edad del Usuario a Actualizar es: " + userData.getAge());

        boolean updateUser = this.userService.patch(id,userData);

        return new ResponseEntity<>(
                updateUser ? "Usuario Actualizado" : "Usuario No Encontrado",
                updateUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(    @PathVariable String id, @RequestBody UserModel user
    ){
        System.out.println("updateUser");
        System.out.println("El id del Usuario a Actualizar en parametro URL es: " + id);
        System.out.println("El id del Usuario a Actualizar es: " + user.getId());
        System.out.println("El nombre del Usuario a Actualizar es: " + user.getName());
        System.out.println("La edad del Usuario a Actualizar es: " + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado, actualizando");

            this.userService.update(user);
        }

        return new ResponseEntity<>(
                user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("El id del usuario a borrar es: " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario Borrado" : "Usuario No Encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


}
