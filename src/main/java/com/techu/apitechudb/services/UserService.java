package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String ord) {
        System.out.println("findAll UserService");

        List<UserModel> result;

        if (ord != null) {
            System.out.println("Se pide ordenacion");
            if (ord.equals("A")) {
                result = this.userRepository.findAll(Sort.by(Sort.Direction.ASC,"age"));
            } else {
                result = this.userRepository.findAll(Sort.by(Sort.Direction.DESC,"age"));
            }
        } else {
            result = this.userRepository.findAll();
        }

        return result;
    }

    public UserModel add(UserModel user) {
        System.out.println("add UserService");

        return this.userRepository.save(user);

    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);

    }

    public boolean patch(String id, UserModel user) {
        System.out.println("patch en userService");
        boolean result = false;

        if(this.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado");
            if (user.getId() == id) {
                System.out.println("Actualizando Usuario");
                this.userRepository.save(user);
            } else {
                user.setId(id);
                System.out.println("Actualizando Usuario sin cambio id");
                this.userRepository.save(user);
            }
            result = true;
        }

        return result;
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, Borrando");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

    public UserModel update(UserModel user) {
        System.out.println("update en UserService");

        return this.userRepository.save(user);
    }
}
